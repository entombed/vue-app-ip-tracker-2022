# IP Address Tracker
Приложение для поиска информации IP-адресов, используя vue 3 и composition API, Tailwind css и leaflet.js.

### API ресурсы
- Geolocation API: [https://geo.ipify.org/](https://geo.ipify.org/)

- Mapbox API: [https://www.mapbox.com/](https://www.mapbox.com/)

### Карта

Leaflet.js: [https://leafletjs.com/](https://leafletjs.com/)

## [Live Demo](https://vue-app-ip-tracker-2022.web.app)
[![](./screen-preview.JPG)](https://vue-app-ip-tracker-2022.web.app)
